/* This file is part of tallyman
Copyright (C) 2018-2021 Sergey Poznyakoff
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
*/
#if HAVE_CONFIG_H
# include <config.h>
#endif
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "grecs.h"
#include "grecs/json.h"

extern int log_facility;
extern char *syslog_tag;
extern char *listen_address;
extern char *pidfile;
extern char *progname;
extern char *runas_user;
extern char *runas_group;
extern unsigned ttl_snmp_table_cache;
extern unsigned ttl_instance_state;
extern char *hostproc_server_addr;
extern char *snmp_client_config_file;

void config_help(void);
void readconfig(char const *file);

void pidfile_remove(void);
void pidfile_create(void);
void pidfile_check(void);

void info(char const *fmt, ...);

void runas(char const *user, char const *group);

struct MHD_Connection;
char *get_remote_ip(struct MHD_Connection *conn);

int service_add(char const *id, grecs_locus_t *loc);
int servdb_update(struct json_value *obj);

u_long servdb_servicesUpTime(void);
size_t servdb_servicesTotal(void);
size_t servdb_servicesRunning(void);
ssize_t servdb_serviceInstances(size_t idx);

void agentx_init(void);
void *agentx_thread(void *p);
void *invalidator_thread(void *p);


