/* This file is part of tallyman
Copyright (C) 2018-2021 Sergey Poznyakoff
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
*/
#if HAVE_CONFIG_H
# include <config.h>
#endif
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include "grecs/json.h"

enum http_method {
	METH_GET,
	METH_POST,
	METH_DELETE,
	METH_PUT,
	METH_OPTIONS,
	METH_INVALID
};

extern int shttp_verbose;

struct shttp_connection *shttp_connect(char *host, struct sockaddr_in *addr);
void shttp_close(struct shttp_connection *conn);
void shttp_request_send(struct shttp_connection *conn, int method, char const *uri);
void shttp_get_reply(struct shttp_connection *conn);

void shttp_request_init_json (struct shttp_connection *conn,
			      struct json_value *obj);


void setprogname(char const *s);
void error(char const *fmt, ...);
void usage_error(char const *fmt, ...);
void system_error(int ec, char const *fmt, ...);

int getdefgw(struct in_addr *ret);
int get_hostname(char **host);

