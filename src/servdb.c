/* This file is part of tallyman
Copyright (C) 2018-2024 Sergey Poznyakoff
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
*/
#include "stevedore.h"
#include <time.h>
#include <pthread.h>
#include <unistd.h>
#include "tallyman_mib.h"

#define MAX_SERVICE 1024

unsigned ttl_instance_state = 60;

struct instance_head {
	struct instance *prev, *next;
};

#define INSTANCE_HEAD_INITIALIZER { NULL, NULL }
#define INSTANCE_HEAD_FIRST(h) ((h)->prev)
#define INSTANCE_NEXT(ip) ((ip)->link.next)
#define INSTANCE_LIST_FOREACH(p,l) \
	for (p = INSTANCE_HEAD_FIRST(l); p; p = INSTANCE_NEXT(p))

struct service {
	char *id;
	struct instance_head head;
	pthread_mutex_t mutex;
};

enum instance_state {
	instance_state_stopped,
	instance_state_running,
	instance_state_expired,
	instance_state_error
};

struct instance {
	char *hostname;
	char *container_id;
	struct service *srv;
	enum instance_state state;
	time_t timestamp;
	char *error_message;
	struct instance_head link;
	pthread_cond_t notbusy;     /* Prevent simultaneous updates */
	int busy:1;                 /* Node is in use if 1 */
};

static inline void
instance_head_init(struct instance_head *head)
{
	head->prev = head->next = NULL;
}
	
static inline void
instance_list_append(struct instance_head *list, struct instance *inst)
{
	inst->link.next = NULL;
	inst->link.prev = list->prev;
	if (list->next)
		list->next->link.next = inst;
	else
		list->prev = inst;
	list->next = inst;
}

static inline void
instance_list_unlink(struct instance_head *list, struct instance *inst)
{
	struct instance *p;

	p = inst->link.prev;
	if (p)
		p->link.next = inst->link.next;
	else
		list->prev = inst->link.next;

	p = inst->link.next;
	if (p)
		p->link.prev = inst->link.prev;
	else
		list->next = inst->link.prev;

	instance_head_init(&inst->link);
}


static struct service service[MAX_SERVICE];
static size_t service_count;

enum {
	SERVICE_TABLE_VALID = 0x01,
	INSTANCE_TABLE_VALID = 0x02
};

static int table_validity_state;
static pthread_mutex_t table_validity_mutex = PTHREAD_MUTEX_INITIALIZER;

static int
table_validity_start(int what)
{
	int result;
	pthread_mutex_lock(&table_validity_mutex);
	result = table_validity_state & what;
	table_validity_state |= what;
	return result;
}

static void
table_validity_end(void)
{
	pthread_mutex_unlock(&table_validity_mutex);
}

static void
table_validity_clear(void)
{
	pthread_mutex_lock(&table_validity_mutex);
	table_validity_state = 0;
	pthread_mutex_unlock(&table_validity_mutex);
}

struct service *
service_by_id(char const *id)
{
	size_t i;

	for (i = 0; i < service_count; i++)
		if (strcmp(service[i].id, id) == 0)
			return &service[i];
	return 0;
}

int
service_add(char const *id, grecs_locus_t *loc)
{
	if (service_by_id(id)) {
		grecs_error(loc, 0, "service %s already registered", id);
		return 1;
	}
	
	if (service_count == MAX_SERVICE) {
		grecs_error(loc, 0, "service table overflow");
		exit(1);
	}

	service[service_count].id = grecs_strdup(id);
	instance_head_init(&service[service_count].head);
	pthread_mutex_init(&service[service_count].mutex, NULL);
	
	service_count++;

	return 0;
}

static struct instance *
instance_alloc(char const *container_id, char const *hostname)
{
	struct instance *inst = calloc(1, sizeof(*inst));

	if (!inst) {
		grecs_error(NULL, 0, "out of memory");
		return NULL;
	}

	if ((inst->container_id = strdup(container_id)) == NULL) {
		free(inst);
		grecs_error(NULL, 0, "out of memory");
		return NULL;
	}

	if ((inst->hostname = strdup(hostname)) == NULL) {
		free(inst->container_id);
		free(inst);
		grecs_error(NULL, 0, "out of memory");
		return NULL;
	}

	instance_head_init(&inst->link);

	inst->busy = 1;
	pthread_cond_init(&inst->notbusy, NULL);
	
	return inst;
}

static void
instance_lock(struct instance *inst)
{
	assert(inst->srv != NULL);
	if (inst->busy) {
		pthread_cond_wait(&inst->notbusy, &inst->srv->mutex);
	}
	inst->busy = 1;
}

static void
instance_unlock(struct instance *inst)
{
	if (inst->busy) {
		inst->busy = 0;
		pthread_cond_broadcast(&inst->notbusy);
	}
}

static void
instance_free(struct instance *inst)
{
	if (inst) {
		pthread_cond_destroy(&inst->notbusy);
		free(inst->hostname);
		free(inst);
	}
}

static inline void
instance_service_link(struct instance *inst, struct service *srv)
{
	inst->srv = srv;
	instance_list_append(&srv->head, inst);
}

static inline void
instance_service_unlink(struct instance *inst)
{
	if (inst->srv)
		instance_list_unlink(&inst->srv->head, inst);
}

static inline int
instance_is_running(struct instance *inst)
{
	return inst->state == instance_state_running;
}
	
static struct instance *
get_service_instance(struct service *srv, char const *id, int hostname)
{
	struct instance *inst;
	INSTANCE_LIST_FOREACH(inst, &srv->head) {
		char *srvid = hostname ? inst->hostname : inst->container_id;
		if (srvid && strcmp(srvid, id) == 0)
			return inst;
	}
	return NULL;
}

static struct json_value *
json_value_lookup_typed(struct json_value *obj, char const *name,
			enum json_value_type type)
{
	struct json_value *v = json_value_lookup(obj, name);
	return (v && v->type == type) ? v : NULL;
}

static int hostproc_service_add(struct service *srv);

int
servdb_update(struct json_value *obj)
{
	struct instance *inst;
	struct service *srv;
	struct json_value *val;
	int status;
	char const *container_id;
	char const *hostname;
	char const *id;
	int new_inst; /* Whether this instance should be considered "new",
			 i.e. a notification about it needs to be sent to
			 hostproc */
	
	val = json_value_lookup_typed(obj, "status", json_bool);
	if (!val)
		return -1;
	status = val->v.b;
	
	val = json_value_lookup_typed(obj, "id", json_string);
	if (!val)
		return -1;
	id = val->v.s;

	val = json_value_lookup_typed(obj, "hostname", json_string);
	if (!val)
		return -1;
	hostname = val->v.s;

	val = json_value_lookup_typed(obj, "container_id", json_string);
	if (val)
		container_id = val->v.s;
	else
		container_id = NULL;

	srv = service_by_id(id);
	if (!srv)
		return -1;

	pthread_mutex_lock(&srv->mutex);
	inst = container_id
		? get_service_instance (srv, container_id, 0)
	        : get_service_instance (srv, hostname, 1);
	if (!inst) {
		inst = instance_alloc(container_id, hostname);
		if (!inst) {
			pthread_mutex_unlock(&srv->mutex);
			return -1;
		}
		instance_service_link(inst, srv);
		new_inst = 1;
	} else {
		instance_lock(inst);
		new_inst = 0;
	}

	time(&inst->timestamp);

	val = json_value_lookup_typed(obj, "error", json_bool);
	if (val && val->v.b) {
		inst->state = instance_state_error;
		val = json_value_lookup_typed(obj, "message", json_string);
		inst->error_message = val ? strdup(val->v.s) : NULL;
	} else {
		inst->state = status
			       ? instance_state_running
			       : instance_state_stopped;
	}
	instance_unlock(inst);

	if (new_inst)
		hostproc_service_add(srv);
	
	pthread_mutex_unlock(&srv->mutex);
	table_validity_clear();
	return 0;
}

size_t
servdb_servicesTotal(void)
{
	return service_count;
}

size_t
servdb_servicesRunning(void)
{
	size_t i, result = 0;
	for (i = 0; i < service_count; i++)
		if (servdb_serviceInstances(i))
			result++;
	return result;
}

ssize_t
servdb_serviceInstances(size_t idx)
{
	struct instance *inst;
	ssize_t result = 0;
	
	if (idx > service_count)
		return -1;
	pthread_mutex_lock(&service[idx].mutex);
	INSTANCE_LIST_FOREACH(inst, &service[idx].head) {
		if (instance_is_running(inst)) {
			result++;
		}
	}
	pthread_mutex_unlock(&service[idx].mutex);
	return result;
}

static int
service_Table_load_row(size_t idx, netsnmp_tdata *table)
{
	netsnmp_tdata_row *data_row;
	struct serviceTable_entry *ent;
	struct service *srv = &service[idx];
		
	ent = SNMP_MALLOC_TYPEDEF(struct serviceTable_entry);
	if (!ent)
		return SNMP_ERR_GENERR;

	data_row = netsnmp_tdata_create_row();
	if (!data_row) {
		SNMP_FREE(ent);
		return SNMP_ERR_GENERR;
	}
	data_row->data = ent;

	ent->serviceIndex = idx;
	ent->serviceName = srv->id;
	ent->serviceName_len = strlen(srv->id);
	ent->serviceInstances = servdb_serviceInstances(idx);

	netsnmp_tdata_row_add_index(data_row, ASN_INTEGER,
				    &ent->serviceIndex,
				    sizeof(ent->serviceIndex));
	netsnmp_tdata_add_row(table, data_row);

	return SNMP_ERR_NOERROR;
}
	
int
serviceTable_load(netsnmp_cache *cache, void *vmagic)
{
	int rc;
	size_t i;
	netsnmp_tdata *table = (netsnmp_tdata *) vmagic;

	if (!table_validity_start(SERVICE_TABLE_VALID)) {
		serviceTable_free(cache, vmagic);
		for (i = 0; i < service_count; i++) {
			if ((rc = service_Table_load_row(i, table)) != SNMP_ERR_NOERROR)
				return rc;
		}
	}
	table_validity_end();
	return 0;
}

void
serviceTable_entry_free(void *data)
{
	/* nothing */
}

#define TMSEC(t) (((t)->tm_hour * 60 + (t)->tm_min) * 60 + (t)->tm_sec)

static int
utc_offset (void)
{
	time_t t = time (NULL);
	struct tm ltm = *localtime (&t);
	struct tm gtm = *gmtime (&t);
	int d = TMSEC (&ltm) - TMSEC (&gtm);
	if (!(ltm.tm_year = gtm.tm_year && ltm.tm_mon == gtm.tm_mon))
		d += 86400;
	return d / 60;
}

static int
setDateAndTime(time_t t, char **pbuf, size_t *plen)
{
	char *buf;
	size_t len = 11;
	struct tm *tm;
	unsigned n;
	
	buf = malloc(len + 1);
	if (!buf) 
		return -1;
	
	tm = localtime(&t);
	/*    A date-time specification.
          
	      field  octets  contents                  range
	      -----  ------  --------                  -----
	        1      1-2   year*                     0..65536
		2       3    month                     1..12
		3       4    day                       1..31
		4       5    hour                      0..23
		5       6    minutes                   0..59
		6       7    seconds                   0..60
		(use 60 for leap-second)
		7       8    deci-seconds              0..9
		8       9    direction from UTC        '+' / '-'
		9      10    hours from UTC*           0..13
		10      11    minutes from UTC          0..59
		
		* Notes:
		- the value of year is in network-byte order
	*/
	n = tm->tm_year % 100;
	buf[0] = n >> 8;
	buf[1] = n & 0xff;
	buf[2] = tm->tm_mon + 1;
	buf[3] = tm->tm_mday;
	buf[4] = tm->tm_hour; 
	buf[5] = tm->tm_min;
	buf[6] = tm->tm_sec;
	buf[7] = '0';
	n = utc_offset();
	if (n < 0) {
		buf[8] = '-';
		n = - n;
	} else 
		buf[8] = '+';
	buf[9] = n / 60;
	buf[10] = n % 60;

	*pbuf = buf;
	*plen = len;

	return 0;
}

static int
instanceTable_entry_load(size_t idx,
			 struct instance *inst, char const *service_id,
			 struct instanceTable_entry *ent)
{
	memset(ent, 0, sizeof(*ent));
	ent->instanceIndex = idx;
	ent->instanceName = strdup(inst->hostname);
	if (!ent->instanceName)
		return -1;
	ent->instanceName_len = strlen(ent->instanceName);
	if (inst->container_id)
		ent->instanceID = strdup(inst->container_id);
	else
		ent->instanceID = strdup("");
	if (!ent->instanceID)
		return -1;
	ent->instanceID_len = strlen(ent->instanceID);
	ent->instanceService = (char*) service_id;
	ent->instanceService_len = strlen(service_id);
	ent->instanceState = inst->state;
	setDateAndTime(inst->timestamp, &ent->instanceTimeStamp,
		       &ent->instanceTimeStamp_len);
	ent->instanceErrorMessage = strdup(inst->error_message
					   ? inst->error_message
					   : "");
	if (!ent->instanceErrorMessage)
		return -1;
	ent->instanceErrorMessage_len = strlen(ent->instanceErrorMessage);
	return 0;
}
	
static int
instanceTable_load_row(size_t idx,
		       struct instance *inst, char const *service_id,
		       netsnmp_tdata *table)
{
	netsnmp_tdata_row *data_row;
	struct instanceTable_entry *ent;
	
	ent = SNMP_MALLOC_TYPEDEF(struct instanceTable_entry);
	if (!ent) 
		return SNMP_ERR_GENERR;

	if (instanceTable_entry_load(idx, inst, service_id, ent)) {
		instanceTable_entry_free(ent);
		SNMP_FREE(ent);
		return SNMP_ERR_GENERR;
	}
		
	data_row = netsnmp_tdata_create_row();
	if (!data_row) {
		instanceTable_entry_free(ent);
		SNMP_FREE(ent);
		return SNMP_ERR_GENERR;
	}
	data_row->data = ent;
	netsnmp_tdata_row_add_index(data_row, ASN_INTEGER,
				    &ent->instanceIndex,
				    sizeof(ent->instanceIndex));
	netsnmp_tdata_add_row(table, data_row);

	return SNMP_ERR_NOERROR;
}

int
instanceTable_load(netsnmp_cache *cache, void *vmagic)
{
	size_t i, idx;
	netsnmp_tdata *table = (netsnmp_tdata *) vmagic;
	int rc = SNMP_ERR_NOERROR;

	if (!table_validity_start(INSTANCE_TABLE_VALID)) {
		instanceTable_free(cache, vmagic);
		idx = 0;
		for (i = 0; i < service_count && rc == SNMP_ERR_NOERROR; i++) {
			struct instance *inst;
		
			pthread_mutex_lock(&service[i].mutex);
			INSTANCE_LIST_FOREACH(inst, &service[i].head) {
				instance_lock(inst);
				rc = instanceTable_load_row(idx, inst,
							    service[i].id,
							    table);
				instance_unlock(inst);
				if (rc != SNMP_ERR_NOERROR)
					break;
				idx++;
			}
			pthread_mutex_unlock(&service[i].mutex);
		}
	}
	table_validity_end();
	return rc;
}

void
instanceTable_entry_free(void *data)
{
	struct instanceTable_entry *ent = data;
	free(ent->instanceName);
	free(ent->instanceID);
	free(ent->instanceErrorMessage);
}

void *
invalidator_thread(void *p)
{
	size_t i;
	struct instance *inst, *next;
	while (1) {
		time_t now = time(NULL);
		for (i = 0; i < service_count; i++) {
			int update = 0;
			pthread_mutex_lock(&service[i].mutex);
			for (inst = INSTANCE_HEAD_FIRST(&service[i].head); inst;) {
				next = INSTANCE_NEXT(inst);
				if (now - inst->timestamp > ttl_instance_state) {
					instance_service_unlink(inst);
					instance_free(inst);
					update = 1;
				}
				inst = next;
			}
			if (update)
				hostproc_service_add(&service[i]);
			pthread_mutex_unlock(&service[i].mutex);
		}
		table_validity_clear();
		sleep(ttl_instance_state);
	}
	return NULL;
}

/* Hostproc notification */

char *hostproc_server_addr;

static void
acc_writer(void *closure, char const *text, size_t len)
{
	grecs_txtacc_grow((struct grecs_txtacc *)closure, text, len);
}

static char *
json_format(struct json_value *obj)
{
	struct grecs_txtacc *acc = grecs_txtacc_create();
	struct json_format fmt = {
		.indent = 0,
		.precision = 0,
		.write = acc_writer,
		.data = acc
	};
	char *s;
	
	json_format_value(obj, &fmt);
	grecs_txtacc_grow_char(acc, 0);
	s = grecs_txtacc_finish(acc, 1);
	grecs_txtacc_free(acc);
	return s;
}

static char const *processGroupMgmtOid = ".1.3.6.1.4.1.9163.104.1.2.0";
static void
my_sess_perror(netsnmp_session *sess)
{
	char           *err;
	snmp_error(sess, NULL, NULL, &err);
	grecs_error(NULL, 0, "%s", err);
	SNMP_FREE(err);
}

int
hostproc_notify(struct json_value *jreq)
{
	netsnmp_session session, *ss;
	netsnmp_pdu *pdu;
	netsnmp_pdu *response;
	char *json_str;
	
	oid hpOID[MAX_OID_LEN];
	size_t hpOID_len;

	int status;
	int rc = -1;
	
	snmp_sess_init(&session);
	session.version =  netsnmp_ds_get_int(NETSNMP_DS_LIBRARY_ID,
                                              NETSNMP_DS_LIB_SNMPVERSION);
	session.peername = hostproc_server_addr;

	ss = snmp_open(&session);
	if (!ss) {
		my_sess_perror(&session);
		//FIXME: any way to free?
		return -1;
	}

	pdu = snmp_pdu_create(SNMP_MSG_SET);
	hpOID_len = MAX_OID_LEN;
	if (!snmp_parse_oid(processGroupMgmtOid, hpOID, &hpOID_len)) {
		snmp_perror(processGroupMgmtOid);
		snmp_close(ss);
		return -1;
	}

	json_str = json_format(jreq);
	snmp_add_var(pdu, hpOID, hpOID_len, 's', json_str);
	free(json_str);

	status = snmp_synch_response(ss, pdu, &response);

	if (status == STAT_SUCCESS && response->errstat == SNMP_ERR_NOERROR) {
		/* Nothing to do */
		rc = 0;
	} else {
		switch (status) {
		case STAT_SUCCESS:
			grecs_error(NULL, 0,
				    "error in packet: %s",
				    snmp_errstring(response->errstat));
			break;
			
		case STAT_TIMEOUT:
			grecs_error(NULL, 0,
				    "timeout: no response from %s",
				    session.peername);
			break;

		default:
			my_sess_perror(ss);
		}
	}

	snmp_close(ss);
	return rc;
}

int
hostproc_service_add(struct service *srv)
{
	struct json_value *obj;
	struct grecs_txtacc *acc;
	struct instance *inst;
	int delim;
	int rc;
	
	if (!hostproc_server_addr)
		return 0;

	obj = json_new_object();
	json_object_set(obj, "name", json_new_string(srv->id));
	json_object_set(obj, "excluded", json_new_bool(1));
	
	acc = grecs_txtacc_create();
	grecs_txtacc_grow_string(acc, "containerd-shim-runc-v2 -namespace moby -id +");
	delim = '(';
	INSTANCE_LIST_FOREACH(inst, &srv->head) {
		if (instance_is_running(inst) && inst->container_id) {
			grecs_txtacc_grow_char(acc, delim);
			delim = '|';
			grecs_txtacc_grow_string(acc, inst->container_id);
		}
	}

	if (delim == '|') {
		grecs_txtacc_grow_char(acc, ')');
		grecs_txtacc_grow_char(acc, 0);
		json_object_set(obj, "pattern",
				json_new_string(grecs_txtacc_finish(acc, 0)));
		json_object_set(obj, "field", json_new_string("cmdline"));
		json_object_set(obj, "match", json_new_string("regex"));
	} else {
		/* Nothing added: create a delete request */
		json_object_set(obj, "delete", json_new_bool(1));
	} 
	grecs_txtacc_free(acc);

	rc = hostproc_notify(obj);
	json_value_free(obj);

	return rc;
}
