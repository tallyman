/* This file is part of tallyman
Copyright (C) 2018-2021 Sergey Poznyakoff
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
*/
#include "stevedore.h"
#include <unistd.h>
#include <errno.h>
#include <pwd.h>
#include <grp.h>

#ifndef SIZE_T_MAX
# define SIZE_T_MAX ((size_t)-1)
#endif

static void
addgid(gid_t **pgv, size_t *pgc, size_t *pgi, gid_t gid)
{
	gid_t *gv = *pgv;
	size_t gc = *pgc;
	size_t gi = *pgi;

	if (gi == gc) {
		if (gc == 0) {
			gc = 16;
			gv = grecs_calloc(gc, sizeof(*gv));
		} else if (gc <= SIZE_T_MAX / 2 / sizeof(*gv)) {
			gc *= 2;
			gv = grecs_realloc(gv, gc * sizeof(*gv));
		} else 
			grecs_alloc_die();
	}
	gv[gi++] = gid;
	*pgv = gv;
	*pgc = gc;
	*pgi = gi;
}

static int
member(gid_t *gv, size_t gc, gid_t gid)
{
	size_t i;

	for (i = 0; i < gc; i++)
		if (gv[i] == gid)
			return 1;
	return 0;
}

static size_t
get_user_groups(const char *user, gid_t **pgv, size_t *pgc)
{
	struct group *gr;
	size_t gi = 0;
	
	setgrent();
	while ((gr = getgrent())) {
		char **p;
		for (p = gr->gr_mem; *p; p++)
			if (strcmp(*p, user) == 0)
				addgid(pgv, pgc, &gi, gr->gr_gid);
	}
	endgrent();
	return gi;
}

void
runas(char const *username, char const *groupname)
{
	struct passwd *pw;
	struct group *gr;
	uid_t uid;
	gid_t gid;
	gid_t *gv;
	size_t gc, gn;
	
	if (!(username || groupname))
		return;
	if (getuid() != 0) {
		grecs_error(NULL, 0, 
                            "not root: can't switch to user privileges");
		exit(1);
	}

	if (!username) {
		pw = getpwuid(0);
		username = "root";
	} else if (username[0] == '+') {
		char *end;
		unsigned long n;

		errno = 0;
		n = strtoul(username + 1, &end, 10);
		if (errno || *end) {
			grecs_error(NULL, 0, "invalid user name %s", username);
			exit(1);
		}

		pw = getpwuid(n);
	} else
		pw = getpwnam(username);

	if (!pw) {
		grecs_error(NULL, 0, "%s: no such user", username);
		exit(1);
	}
	username = pw->pw_name;
		
	uid = pw->pw_uid;
	gid = pw->pw_gid;
	
	if (groupname) {
		if (groupname[0] == '+') {
			char *end;
			unsigned long n;

			errno = 0;
			n = strtoul(groupname + 1, &end, 10);
			if (errno || *end) {
				grecs_error(NULL, 0, "invalid group name %s", 
                                            groupname);
				exit(1);
			}

			gr = getgrgid(n);
		} else
			gr = getgrnam(groupname);

		if (!gr) {
			grecs_error(NULL, 0, "%s: no such group", groupname);
			exit(1);
		}

		gid = gr->gr_gid;
	}

	gv = NULL;
	gc = 0;
	gn = get_user_groups(username, &gv, &gc);
	if (!member(gv, gn, gid))
		addgid(&gv, &gc, &gn, gid);

	/* Reset group permissions */
	if (setgroups(gn, gv)) {
		grecs_error(NULL, errno, "setgroups");
		exit(1);
	}
	free(gv);
	
	if (gid) {
		/* Switch to the user's gid. */
		if (setgid(gid)) {
			grecs_error(NULL, errno, "setgid(%lu)",
			            (unsigned long) gid);
			exit(1);
		}
	}
	
	/* Now reset uid */
	if (uid) {
		if (setuid(uid)) {
			grecs_error(NULL, errno, "setuid(%lu) failed",
			            (unsigned long) uid);
			exit(1);
		}
	}
}	
