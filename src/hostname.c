/* This file is part of tallyman
Copyright (C) 2018-2021 Sergey Poznyakoff
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
*/

#if HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>

#ifndef MAXHOSTNAMELEN
# define MAXHOSTNAMELEN 64
#endif

int
get_hostname(char **host)
{
	char *hostname = NULL;
	size_t size = 0;
	char *p;
	struct hostent *hp;
	
	while (1) {
		if (size == 0) {
			size = MAXHOSTNAMELEN;
			p = malloc(size);
		} else {
			if ((size_t)-1 / 3 * 2 <= size) {
				p = NULL;
			} else {
				size += (size + 1) / 2;
				p = realloc(hostname, size);
			}
		}
		if (!p)	{
			free(hostname);
			return ENOMEM;
		}
		hostname = p;
		hostname[size - 1] = 0;
		if (gethostname(hostname, size - 1) == 0) {
			if (!hostname[size - 1])
				break;
		} else if (errno != 0
			   && errno != ENAMETOOLONG
			   && errno != EINVAL
			   && errno != ENOMEM) {
			int rc = errno;
			free (hostname);
			return rc;
		}
	}

	/* Try to return fully qualified host name */
	hp = gethostbyname (hostname);
	if (hp) {
		size_t len = strlen(hp->h_name);
		if (size < len + 1) {
			p = realloc(hostname, len + 1);
			if (!p)	{
				free(hostname);
				return ENOMEM;
			}
			hostname = p;
		}
		strcpy(hostname, hp->h_name);
	}

	*host = hostname;
	return 0;
}  

