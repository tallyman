/* This file is part of tallyman
Copyright (C) 2018-2021 Sergey Poznyakoff
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
*/
#ifndef SYSCONFDIR
# define SYSCONFDIR "/etc"
#endif

#define DEFAULT_CONFIG_FILE_NAME SYSCONFDIR "/stevedore.conf"

#ifndef DEFAULT_ADDRESS
# define DEFAULT_ADDRESS "0.0.0.0"
#endif

#ifndef DEFAULT_PORT
# define DEFAULT_PORT "8990"
#endif

