/* This file is part of tallyman
Copyright (C) 2018-2021 Sergey Poznyakoff
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
*/
#include "tallyman.h"
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include "grecs.h"
#include "wordsplit.h"

int shttp_verbose;

char *method_names[] = {
	[METH_GET]     = "GET",
	[METH_POST]    = "POST",
	[METH_DELETE]  = "DELETE",
	[METH_PUT]     = "PUT",
	[METH_OPTIONS] = "OPTIONS",
	NULL
};

static char http_version[] = "HTTP/1.1";

struct shttp_io {
	int code;
	struct grecs_symtab *headers;
	unsigned long content_length;
	char *content;
};

struct shttp_buf {
	char *base;
	size_t size;
};

struct shttp_connection {
	FILE *fp;
	char *host;
	struct shttp_io req;
	struct shttp_io resp;
	struct shttp_buf sendbuf;
	struct shttp_buf readbuf;
	char *b64auth;
	char *status_line[3];
	struct json_value *result;
};

struct http_header {
	char *name;
	char *value;
};

static unsigned
http_header_hash(void *data, unsigned long n_buckets)
{
	const struct http_header *p = data;
	return grecs_hash_string_ci(p->name, n_buckets);
}

static int
http_header_compare(void const *data1, void const *data2)
{
	const struct http_header *p1 = data1;
	const struct http_header *p2 = data2;
	return strcasecmp(p1->name, p2->name);
}

static int
http_header_copy(void *a, void *b)
{
	struct http_header *dst = a;
	struct http_header *src = b;

	dst->name = grecs_strdup(src->name);
	dst->value = src->value ? grecs_strdup(src->value) : NULL;

	return 0;
}

static void
http_header_free(void *p)
{
	const struct http_header *h = p;
	grecs_free(h->name);
	grecs_free(h->value);
	free(p);
}

static void
shttp_io_init(struct shttp_io *io)
{
	io->code = 0;
	io->content_length = 0;
	grecs_free(io->content);
	io->content = NULL;
	grecs_symtab_clear(io->headers);
}

static int
shttp_io_header_put(struct shttp_io *io, char const *name, char const *fmt, ...)
{
	int install = 1;
	struct http_header key, *ret;
	va_list ap;
	size_t len = 0;

	if (!io->headers)
		io->headers = grecs_symtab_create(sizeof(struct http_header),
						  http_header_hash,
						  http_header_compare,
						  http_header_copy,
						  NULL, http_header_free);
	key.name = (char *)name;
	key.value = NULL;
	ret = grecs_symtab_lookup_or_install(io->headers, &key, &install);
	if (!ret) {
		grecs_error(NULL, errno, "cannot install header %s",
			    name);
		return 1;
	}
	if (!install)
		grecs_free(ret->value);
	va_start(ap, fmt);
	grecs_vasprintf(&ret->value, &len, fmt, ap);
	va_end(ap);
	return 0;
}

static char *
shttp_io_header_get(struct shttp_io *io, char const *name)
{
	struct http_header key, *ret;

	if (!io->headers)
		return NULL;
	key.name = (char *)name;
	ret = grecs_symtab_lookup_or_install(io->headers, &key, NULL);
	return ret ? ret->value : NULL;
}

static void
shttp_io_free(struct shttp_io *io)
{
	grecs_symtab_free(io->headers);
	grecs_free(io->content);
}

struct shttp_connection *
shttp_connect(char *host, struct sockaddr_in *addr)
{
	int fd;
	int flags;
	FILE *fp;
	struct shttp_connection *conn;
	
	fd = socket(addr->sin_family, SOCK_STREAM, 0);
	if (fd == -1) {
		grecs_error(NULL, errno, "socket");
		return NULL;
	}

	if ((flags = fcntl(fd, F_GETFD, 0)) == -1
	    || fcntl(fd, F_SETFD, flags | FD_CLOEXEC) == -1)
		grecs_error(NULL, errno, "cannot set close-on-exec");

	if (connect(fd, (struct sockaddr*)addr, sizeof(*addr))) {
		grecs_error(NULL, errno, "connect");
		close(fd);
		return NULL;
	}

	fp = fdopen(fd, "w+");
	if (!fp) {
		grecs_error(NULL, errno, "fdopen");
		close(fd);
		return NULL;
	}

	conn = grecs_zalloc(sizeof(*conn));
	conn->host = grecs_strdup(host);
	conn->fp = fp;
	return conn;
}

static void
shttp_auth_free(struct shttp_connection *conn)
{
	if (conn->b64auth) {
		memset(conn->b64auth, 0, strlen(conn->b64auth));
		free(conn->b64auth);
		conn->b64auth = NULL;
	}
}

static void
shttp_status_line_free(struct shttp_connection *conn)
{
	free(conn->status_line[0]);
	free(conn->status_line[1]);
	free(conn->status_line[2]);
	conn->status_line[0] = NULL;
	conn->status_line[1] = NULL;
	conn->status_line[2] = NULL;
}

void
shttp_close(struct shttp_connection *conn)
{
	fclose(conn->fp);
	free(conn->host);
	shttp_io_free(&conn->req);
	shttp_io_free(&conn->resp);
	grecs_free(conn->sendbuf.base);
	grecs_free(conn->readbuf.base);
	shttp_status_line_free(conn);
	shttp_auth_free(conn);
	json_value_free(conn->result);
	grecs_free(conn);
}

static void
shttp_send_line(struct shttp_connection *conn, char const *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	grecs_vasprintf(&conn->sendbuf.base, &conn->sendbuf.size, fmt, ap);
	va_end(ap);
	if (shttp_verbose > 1)
		printf("> %s\n", conn->sendbuf.base);
	fprintf(conn->fp, "%s\r\n", conn->sendbuf.base);
}

static int
shttp_read_line(struct shttp_connection *conn)
{
	ssize_t rc;
	rc = grecs_getline(&conn->readbuf.base, &conn->readbuf.size, conn->fp);
	if (rc > 0) {
		if (conn->readbuf.base[--rc] == '\n') {
			if (rc > 0 && conn->readbuf.base[rc - 1] == '\r')
				--rc;
			conn->readbuf.base[rc] = 0;
		}
		if (shttp_verbose > 1)
			printf("< %s\n", conn->readbuf.base);
		return 0;
	} else if (rc == 0) {
		if (shttp_verbose > 1)
			printf("<EOF>\n");
		return 1;
	}
	grecs_error(NULL, errno, "HTTP read error");
	exit(1);
}

static int
send_header(void *sym, void *data)
{
	struct http_header *h = sym;
	struct shttp_connection *conn = data;
	shttp_send_line(conn, "%s: %s", h->name, h->value);
	return 0;
}

void
shttp_request_send(struct shttp_connection *conn, int method, char const *uri)
{
	shttp_send_line(conn, "%s %s %s", method_names[method], uri,
			http_version);
	shttp_send_line(conn, "Host: %s", conn->host);

	if (conn->b64auth)
		shttp_send_line(conn, "Authorization: Basic %s", conn->b64auth);

	grecs_symtab_foreach(conn->req.headers, send_header, conn);
	if (conn->req.content_length) {
		shttp_send_line(conn, "Content-Length: %lu",
				conn->req.content_length);
		shttp_send_line(conn, "Content-Type: application/json");
	}
	shttp_send_line(conn, "");
	if (conn->req.content_length) {
		size_t s =
		    fwrite(conn->req.content, 1, conn->req.content_length,
			   conn->fp);
		if (s != conn->req.content_length) {
			grecs_error(NULL, 0,
				    "wrote %lu of %lu bytes of content",
				    (unsigned long)s, conn->req.content_length);
			exit(1);
		}
	}
}

#define ISWS(c) ((c) == ' ' || (c) == '\t')

static void
shttp_save_header(struct shttp_connection *conn)
{
	char *p;

	for (p = conn->readbuf.base; *p; p++)
		if (*p == ':') {
			*p++ = 0;
			p += strspn(p, " \t");
			shttp_io_header_put(&conn->resp, conn->readbuf.base,
					    "%s", p);
			return;
		}

	grecs_error(NULL, 0, "malformed header line: %s",
		    conn->readbuf.base);
	exit(1);
}

void
shttp_get_reply(struct shttp_connection *conn)
{
	enum input_state
	    { is_initial, is_headers, is_content } state = is_initial;
	char const *val;

	shttp_io_init(&conn->resp);
	shttp_status_line_free(conn);
	json_value_free(conn->result);
	conn->result = NULL;

	while (state != is_content) {
		if (shttp_read_line(conn))
			break;
		if (state == is_initial) {
			unsigned long n;
			char *p;
			int rc;
			struct wordsplit ws;

			ws.ws_delim = " ";
			ws.ws_options = WRDSO_MAXWORDS;
			ws.ws_maxwords = 3;
			rc = wordsplit(conn->readbuf.base,
				       &ws,
				       WRDSF_DELIM|WRDSF_OPTIONS);
			if (rc) {
				grecs_error(NULL, 0, "wordsplit: %s",
					    wordsplit_strerror(&ws));
				exit(1);
			} else if (ws.ws_wordc < 2) {
				grecs_error(NULL, 0, "unexpected reply: %s",
					    conn->readbuf.base);
				exit(1);
			}
			conn->status_line[0] = ws.ws_wordv[0];
			conn->status_line[1] = ws.ws_wordv[1];
			if (ws.ws_wordc == 3)
				conn->status_line[2] = ws.ws_wordv[2];
			ws.ws_wordc = 0;
			wordsplit_free(&ws);
			
			if (strcmp(conn->status_line[0], http_version)) {
				grecs_error(NULL, 0,
					    "unsupported HTTP version: %s",
					    conn->status_line[0]);
				exit(1);
			}
			n = strtoul(conn->status_line[1], &p, 0);
			if (*p || n > 599) {
				grecs_error(NULL, 0, "bad reply status: %s",
					    conn->readbuf.base);
				exit(1);
			}
			conn->resp.code = n;

			state = is_headers;
		} else if (state == is_headers) {
			if (conn->readbuf.base[0])
				shttp_save_header(conn);
			else
				state = is_content;
		}
	}

	val = shttp_io_header_get(&conn->resp, "content-length");
	if (val) {
		ssize_t rc;
		char *p;
		unsigned long len = strtoul(val, &p, 10);

		if (*p) {
			grecs_error(NULL, 0, "bad content length: %s", val);
			exit(1);
		}

		if (len) {
			conn->resp.content = grecs_malloc(len + 1);
			conn->resp.content_length = len;
			rc = fread(conn->resp.content, len, 1, conn->fp);
			if (rc != 1) {
				grecs_error(NULL, ferror(conn->fp) ? errno : 0,
					    "content read error");
				exit(1);
			}

			val = shttp_io_header_get(&conn->resp, "content-type");
			if (!val)
				grecs_error(NULL, 0,
					    "no content type specified");
			else if (strcmp(val, "application/json"))
				grecs_error(NULL, 0,
					    "unsupported content type: %s",
					    val);
			else {
				conn->result =
				    json_parse_string(conn->resp.content,
						      conn->
						      resp.content_length);
				if (!conn->result) {
					grecs_error(&json_err_locus, 0, "%s",
						    json_err_diag);
					grecs_error(NULL, 0,
						    "original json was: %s",
						    conn->resp.content);
					grecs_error(NULL, 0,
						    "original status line: %s %s %s",
						    conn->status_line[0],
						    conn->status_line[1],
						    conn->status_line[2]);
					exit(1);
				}
			}
		}
	}
}

static void
acc_writer(void *closure, char const *text, size_t len)
{
	grecs_txtacc_grow((struct grecs_txtacc *)closure, text, len);
}

static void
json_dump(struct shttp_io *io, struct json_value *obj)
{
	struct grecs_txtacc *acc = grecs_txtacc_create();
	struct json_format fmt = {
		.indent = 0,
		.precision = 0,
		.write = acc_writer,
		.data = acc
	};
	
	json_format_value(obj, &fmt);
	grecs_txtacc_grow_char(acc, 0);
	io->content = grecs_txtacc_finish(acc, 1);
	io->content_length = strlen(io->content);
	grecs_txtacc_free(acc);
}

void
shttp_request_init_json (struct shttp_connection *conn, struct json_value *obj)
{
	shttp_io_init(&conn->req);
	json_dump(&conn->req, obj);
}


