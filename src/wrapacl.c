/* This file is part of tallyman
Copyright (C) 2018-2021 Sergey Poznyakoff
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
*/
#include <tcpd.h>
#include <microhttpd.h>

int
stv_acl(void *cls, const struct sockaddr *addr,  socklen_t addrlen)
{
	struct request_info req;
	request_init(&req,
		     RQ_DAEMON, "tallymand",
		     RQ_CLIENT_SIN, addr,
		     RQ_SERVER_SIN, cls,
		     NULL);
	sock_methods(&req);
	return hosts_access(&req) ? MHD_YES : MHD_NO;
}
