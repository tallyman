/* This file is part of tallyman
Copyright (C) 2018-2021 Sergey Poznyakoff
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
*/
#include "stevedore.h"
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>

void
pidfile_remove(void)
{
	if (pidfile && unlink(pidfile))
		grecs_error(NULL, errno, "cannot unlink pidfile `%s'", pidfile);
}

void
pidfile_create(void)
{
	FILE *fp;

	if (!pidfile)
		return;

	fp = fopen(pidfile, "w");
	if (!fp) {
		grecs_error(NULL, errno, "cannot create pidfile `%s'", pidfile);
		exit(1);
	}
	fprintf(fp, "%lu", (unsigned long) getpid());
	fclose(fp);
}

/* Check whether pidfile exists and if so, whether its PID is still
   active. Exit if it is. */
void
pidfile_check(void)
{
	unsigned long pid;
	FILE *fp;

	if (!pidfile)
		return;

	fp = fopen(pidfile, "r");

	if (fp) {
		if (fscanf(fp, "%lu", &pid) != 1) {
			grecs_error(NULL, 0, "cannot get pid from pidfile `%s'",
                                    pidfile);
		} else {
			if (kill(pid, 0) == 0) {
				grecs_error(NULL, 0,
                                      "%s appears to run with pid %lu. "
				      "If it does not, remove `%s' and retry.",
				      progname, pid, pidfile);
				exit(1);
			}
		}

		fclose(fp);
		if (unlink(pidfile)) {
			grecs_error(NULL, errno, "cannot unlink pidfile `%s'",
			            pidfile);
			exit(1);
		}
	} else if (errno != ENOENT) {
		grecs_error(NULL, errno, "cannot open pidfile `%s'",
		            pidfile);
		exit(1);
	}
}
