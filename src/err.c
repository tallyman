/* This file is part of tallyman
Copyright (C) 2018-2021 Sergey Poznyakoff
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <grecs.h>

char *progname;

static void
tallyman_print_diag(grecs_locus_t const *locus, int err, int errcode,
		    const char *msg)
{
	fflush(stdout);
	fprintf(stderr, "%s: ", progname);
	if (locus) {
		YY_LOCATION_PRINT(stderr, *locus);
		fputc(':', stderr);
		fputc(' ', stderr);
	}
	if (!err)
		fprintf(stderr, "warning: ");
	fprintf(stderr, "%s", msg);
	if (errcode)
		fprintf(stderr, ": %s", strerror(errcode));
	fputc('\n', stderr);
}


void
setprogname(char const *s)
{
	progname = grecs_strdup(s);
	grecs_print_diag_fun = tallyman_print_diag;
}

void
error(char const *fmt, ...)
{
	va_list ap;
	fprintf(stderr, "%s: ", progname);
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	fputc('\n', stderr);
}

void
usage_error(char const *fmt, ...)
{
	va_list ap;
	fprintf(stderr, "%s: ", progname);
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	fputc('\n', stderr);
	exit(1);
}

void
system_error(int ec, char const *fmt, ...)
{
	va_list ap;
	fprintf(stderr, "%s: ", progname);
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	fprintf(stderr, ": %s\n", strerror(ec));
}

	
	
		
