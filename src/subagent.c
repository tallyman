/* This file is part of tallyman
Copyright (C) 2018-2021 Sergey Poznyakoff
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
*/
#include "stevedore.h"
#include <time.h>
#include <pthread.h>
#include "tallyman_mib.h"
#include <signal.h>

char *snmp_client_config_file;

void
agentx_init(void)
{
	if (grecs_log_to_stderr) {
		snmp_enable_stderrlog();
	} else {
		snmp_enable_syslog_ident(progname, LOG_DAEMON);
                snmp_disable_stderrlog();
	}
	netsnmp_ds_set_boolean(NETSNMP_DS_APPLICATION_ID,
			       NETSNMP_DS_AGENT_ROLE, 1);
	if (snmp_client_config_file) {
		netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID,
				       NETSNMP_DS_LIB_DONT_READ_CONFIGS,
				       1);
	}
	SOCK_STARTUP;
	init_agent("stevedore");
	init_tallyman_mib();
	init_snmp("stevedore");
	if (snmp_client_config_file) {
		read_config_with_type(snmp_client_config_file, "snmp");
	}
	netsnmp_ds_set_boolean(NETSNMP_DS_LIBRARY_ID,
                               NETSNMP_DS_LIB_DONT_PERSIST_STATE, 1);
}

time_t start_time;

u_long
servdb_servicesUpTime(void)
{
	return time(NULL) - start_time;
}

void *
agentx_thread(void *p)
{
	start_time = time(NULL);
	while (1) {
		agent_check_and_process(1);
	}
}
