/* This file is part of tallyman
Copyright (C) 2018-2021 Sergey Poznyakoff
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
*/
#include "tallyman.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <errno.h>
#include <getopt.h>
#include <sys/time.h>
#include <netdb.h>
#include <signal.h>
#include <setjmp.h>
#include "runcap.h"
#include "defs.h"

enum {
	EX_OK = 0,
	EX_FAILURE = 1
};

enum {
	OPT_CONNECTION_TIMEOUT = 256,
	OPT_EXECUTION_TIMEOUT
};

struct option longopts[] = {
	{ "help",     no_argument,       0, '?' },
	{ "server",   required_argument, 0, 's' },
	{ "hostname", required_argument, 0, 'h' },
	{ "value",    required_argument, 0, 'v' },
	{ "debug",    no_argument,       0, 'd' },
	{ "connection-timeout",  required_argument, 0, OPT_CONNECTION_TIMEOUT },
	{ "execution-timeout",   required_argument, 0, OPT_EXECUTION_TIMEOUT },
	{ "version",  no_argument,       0, 'V' },	
        { "quiet",    no_argument,       0, 'q' },
	{ NULL }
};
static char shortopts[] = "+?ds:h:qv:V";
		    
void
help(void)
{
	printf("usage: tallyman [OPTIONS] [--] ID COMMAND [ARGS...]\n");
	printf("\n");
	printf("OPTIONS are:\n\n");
	printf("   -d, --debug              increase debug verbosity\n");
	printf("   -h, --hostname=NAME      set this server hostname\n");
	printf("   -q, --quiet              don't display COMMAND's stderr;\n");
	printf("                            when used twice, don't display COMMAND's\n");
	printf("                            stdout either\n");
	printf("   -s, --server=HOST:PORT   remote host server\n");
	printf("   -v, --value=JSON         add JSON object to the report\n");
	printf("       --connection-timeout=SECONDS\n");
	printf("                            set connection timeout\n");
	printf("       --execution-timeout=SECONDS\n");
	printf("                            set execution timeout\n");
	printf("   -V, --version            display version number and license\n");
	printf("   -?, --help               display this help text\n\n");
}

#include "gpl3.h"
void
version(void)
{
	printf("tallyman (%s) %s\n", PACKAGE_TARNAME, PACKAGE_VERSION);
	printf("%s\n", license);
}	

static int
get_timeout(char const *arg)
{
	unsigned long n;
	char *p;
	errno = 0;
	n = strtoul(arg, &p, 10);
	if (errno || *p || n > INT_MAX) 
		usage_error("invalid timeout: %s", arg);
	return (int) n;
}

char *server_host;
struct sockaddr_in server_addr;
int exec_timeout = 5;
int connection_timeout = 5;

static void
parse_server_address(char *server)
{
	struct addrinfo hints;
	struct addrinfo *res;
	char addrbuf[INET_ADDRSTRLEN];
	char *p;
	int rc;
	
	if (server) {
		p = strchr(server, ':');
		if (p) {
			*p++ = 0;
			if (server[0] == 0)
				server = NULL;
		} else {
			p = DEFAULT_PORT;
		}
	} else {
		p = DEFAULT_PORT;
	}

	if (server == NULL) {
		struct in_addr a;
		
		if (getdefgw(&a)) {
			system_error(errno, "getdefgw");
			exit(1);
		}
		inet_ntop(AF_INET, &a, addrbuf, sizeof(addrbuf));
		server = addrbuf;
	}
	
	memset(&hints, 0, sizeof (hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	rc = getaddrinfo(server, p, &hints, &res);
	if (rc) {
		error("%s: %s", server, gai_strerror(rc));
		exit(1);
	}
	memcpy(&server_addr, res->ai_addr, res->ai_addrlen);
	freeaddrinfo(res);
	server_host = grecs_malloc(strlen(server) + strlen(p) + 2);
	strcat(strcat(strcpy(server_host, server), ":"), p);
}

void
capture(struct json_value *obj, char const *varname, struct runcap *rc,
	int sn)
{
	struct json_value *ar;
	char *buf = NULL;
	size_t size = 0;
	int res;
	
	ar = json_new_array();
	while ((res = runcap_getline(rc, sn, &buf, &size)) > 0) {
		json_array_append(ar, json_new_string(buf));
	}
	json_array_flatten(ar);
	json_object_set(obj, varname, ar);
}

static void
printer(void *d, char const *buf, size_t size)
{
	FILE *fp = d;
	fwrite(buf, size, 1, fp);
}

struct json_format fmt = { 0, -1, printer, NULL };

void
json_dump(struct json_value *obj)
{
	fmt.data = stdout;
	json_format_value(obj, &fmt);
	putchar('\n');
}

static jmp_buf jmpbuf;

static void
sig_alarm(int sig)
{
	longjmp(jmpbuf, 1);
}

static void
stdout_linemon(const char *bufptr, size_t bufsize, void *closure)
{
	fwrite(bufptr, bufsize, 1, stdout);
}

static void
stderr_linemon(const char *bufptr, size_t bufsize, void *closure)
{
	fwrite(bufptr, bufsize, 1, stderr);
}

char *
getcontid(void)
{
	FILE *fp;
	char *id;
	
	fp = fopen("/proc/self/cgroup", "r");
	if (!fp)
		return NULL;
	if (fscanf(fp, "%*d:%*[^:]:/%*[^/]/%ms\n", &id) != 1)
		id = NULL;
	fclose(fp);
	return id;
}

int
main(int argc, char **argv)
{
	int c, i;
	char *hostname = NULL;
	char *id;
	char *server = 0;
	char *value = NULL;
	char *service_id;
	struct json_value *obj, *ar;
	struct timeval tv;
	struct timezone tz;
	struct runcap rc;
	struct shttp_connection *shttp;
	int status;
	int rcflags = RCF_TIMEOUT|RCF_STDOUT_LINEMON|RCF_STDERR_LINEMON;
	
	setprogname(argv[0]);
	while ((c = getopt_long(argc, argv, shortopts, longopts, NULL))
	       != EOF) {
		switch (c) {
		case '?':
			help();
			exit(0);
		case 's':
			server = optarg;
			break;
		case 'h':
			hostname = optarg;
			break;
		case 'v':
			value = optarg;
			break;
		case 'd':
			shttp_verbose++;
			break;
		case 'V':
			version();
			exit(0);
		case 'q':
			if (rcflags & RCF_STDOUT_LINEMON)
				rcflags &= ~RCF_STDOUT_LINEMON;
			else
				rcflags &= ~RCF_STDERR_LINEMON;
			break;
		case OPT_CONNECTION_TIMEOUT:
			exec_timeout = get_timeout(optarg);
			break;
		case OPT_EXECUTION_TIMEOUT:
			connection_timeout = get_timeout(optarg);
			break;
		default:
			exit(1);
		}
	}
	argc -= optind;
	argv += optind;

	if (argc == 0)
		usage_error("required argument missing");

	service_id = *argv;
	argc--;
	argv++;

	if (argc == 0)
		usage_error("no program given");

	if (*service_id == 0) {
		execvp(argv[0], argv);
		system_error(errno, "failed to run %s", argv[0]);
		exit(127);
	}
	
	if (!hostname) {
		if (get_hostname(&hostname)) {
			system_error(errno, "hostname");
			exit(1);
		}
	}

	parse_server_address(server);

	if (value) {
		obj = json_parse_string (value, strlen (value));
		if (!obj) {
			grecs_error(&json_err_locus, 0, "%s", json_err_diag);
			exit(1);
		}
	} else {
		obj = json_new_object();
	}
	json_object_set(obj, "id", json_new_string(service_id));
	json_object_set(obj, "hostname", json_new_string(hostname));
	if ((id = getcontid()) != NULL)
		json_object_set(obj, "container_id", json_new_string(id));
	gettimeofday(&tv, &tz);
	json_object_set(obj, "timestamp",
			json_new_number(tv.tv_sec + (double)tv.tv_usec / 1e6));

	ar = json_new_array();
	for (i = 0; i < argc; i++) {
		json_array_append(ar, json_new_string(argv[i]));
	}
	json_array_flatten(ar);
	json_object_set(obj, "command", ar);

	rc.rc_argv = argv;
	rc.rc_timeout = exec_timeout;
	if (rcflags & RCF_STDOUT_LINEMON)
		rc.rc_cap[RUNCAP_STDOUT].sc_linemon = stdout_linemon;
	if (rcflags & RCF_STDERR_LINEMON)
		rc.rc_cap[RUNCAP_STDERR].sc_linemon = stderr_linemon;
	
	status = EX_FAILURE;
	if (runcap(&rc, rcflags)) {
		char const *emsg = strerror(errno);
		json_object_set(obj, "status", json_new_bool(0));
		json_object_set(obj, "error", json_new_bool(1));
		json_object_set(obj, "message", json_new_string(emsg));
		error("failed to run %s: %s", argv[0], emsg);
	} else {
		if (WIFEXITED(rc.rc_status)) {
			status = WEXITSTATUS(rc.rc_status);
			json_object_set(obj, "status",
				json_new_bool(status == 0));
			if (status
			    && rc.rc_cap[RUNCAP_STDERR].sc_nlines == 0) {
				if (status == 127)
					error("failed to run %s: %s", argv[0],
					      "program not found");
				else
					error("command %s exited with status %d",
					      argv[0], status);
			}
		} else {
			char *msg = 0;
			size_t siz = 0;

			if (WIFSIGNALED(rc.rc_status)) {
				grecs_asprintf(&msg, &siz,
					    "command terminated on signal %d",
					    WTERMSIG(rc.rc_status));
			} else if (WIFSTOPPED(rc.rc_status)) {
				grecs_asprintf(&msg, &siz,
					       "command got stopped");
			} else {
				grecs_asprintf(&msg, &siz,
					       "command status unrecognized");
			}
				
			json_object_set(obj, "status", json_new_bool(0));
			json_object_set(obj, "error", json_new_bool(1));
			json_object_set(obj, "message", json_new_string(msg));

			error("%s: %s", argv[0], msg);
			free(msg);
		}

		if (rc.rc_cap[RUNCAP_STDOUT].sc_nlines)
			capture(obj, "stdout", &rc, RUNCAP_STDOUT);
		if (rc.rc_cap[RUNCAP_STDERR].sc_nlines)
			capture(obj, "stderr", &rc, RUNCAP_STDERR);
	}

	//json_dump(obj);

	if (setjmp(jmpbuf) == 0) {
		signal(SIGALRM, sig_alarm);
		alarm(connection_timeout);
		shttp = shttp_connect(server_host, &server_addr);
		if (shttp) {
			shttp_request_init_json(shttp, obj);
			shttp_request_send(shttp, METH_POST, "/");
			//FIXME: reply
		}
	} else
		error("%s: connection timeout", server_host);
	
	return status;
}
