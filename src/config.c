/* This file is part of tallyman
Copyright (C) 2018-2021 Sergey Poznyakoff
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
*/
#include "stevedore.h"
#include <stdio.h>
#include <syslog.h>
#include "defs.h"

int log_facility = LOG_DAEMON;
char *syslog_tag;
char *listen_address;
char *pidfile;
char *runas_user;
char *runas_group;

struct keyword {
	char *name;
	int tok;
};

static int
keyword_to_tok(const char *str, struct keyword *kw, int *pres)
{
	for (; kw->name; kw++)
		if (strcmp(kw->name, str) == 0) {
			*pres = kw->tok;
			return 0;
		}
	return 1;
}

static int
tok_to_keyword(int tok, struct keyword *kw, const char **pres)
{
	for (; kw->name; kw++)
		if (kw->tok == tok) {
			*pres = kw->name;
			return 0;
		}
	return 1;
}

static struct keyword kwfac[] = {
	{ "USER",    LOG_USER },
	{ "DAEMON",  LOG_DAEMON },
	{ "AUTH",    LOG_AUTH },
	{ "AUTHPRIV",LOG_AUTHPRIV },
	{ "MAIL",    LOG_MAIL },
	{ "CRON",    LOG_CRON },
	{ "LOCAL0",  LOG_LOCAL0 },
	{ "LOCAL1",  LOG_LOCAL1 },
	{ "LOCAL2",  LOG_LOCAL2 },
	{ "LOCAL3",  LOG_LOCAL3 },
	{ "LOCAL4",  LOG_LOCAL4 },
	{ "LOCAL5",  LOG_LOCAL5 },
	{ "LOCAL6",  LOG_LOCAL6 },
	{ "LOCAL7",  LOG_LOCAL7 },
	{ NULL }
};

static int
stv_strtofac(const char *str)
{
	int res;
	if (keyword_to_tok(str, kwfac, &res))
		return -1;
	return res;
}

#if 0
static struct keyword kwpri[] = {
	{ "EMERG", LOG_EMERG },
	{ "ALERT", LOG_ALERT },
	{ "CRIT", LOG_CRIT },
	{ "ERR", LOG_ERR },
	{ "WARNING", LOG_WARNING },
	{ "NOTICE", LOG_NOTICE },
	{ "INFO", LOG_INFO },
	{ "DEBUG", LOG_DEBUG },
	{ NULL }
};

static int
stv_strtopri(const char *str)
{
	int res;
	if (keyword_to_tok(str, kwpri, &res))
		return -1;
	return res;
}

static const char *
stv_pritostr(int pri)
{
	const char *res;
	if (tok_to_keyword(pri, kwpri, &res))
		return NULL;
	return res;
}

static const char *
stv_factostr(int fac)
{
	const char *res;
	if (tok_to_keyword(fac, kwfac, &res))
		return NULL;
	return res;
}
#endif

static int
stv_assert_string_arg(grecs_locus_t *locus,
		     enum grecs_callback_command cmd,
		     const grecs_value_t *value)
{
	if (cmd != grecs_callback_set_value) {
		grecs_error(locus, 0, "Unexpected block statement");
		return 1;
	}
	if (!value || value->type != GRECS_TYPE_STRING) {
		grecs_error(&value->locus, 0,
			    "expected scalar value as a tag");
		return 1;
	}
	return 0;
}

static int
cb_syslog_facility(enum grecs_callback_command cmd, grecs_node_t *node,
		   void *varptr, void *cb_data)
{
	grecs_locus_t *locus = &node->locus;
	grecs_value_t *value = node->v.value;
	int fac;
	
	if (stv_assert_string_arg(locus, cmd, value))
		return 1;

	fac = stv_strtofac(value->v.string);
	if (fac == -1)
		grecs_error(&value->locus, 0,
			    "Unknown syslog facility `%s'",
			    value->v.string);
	else
		*(int*)varptr = fac;
	return 0;
}

static struct grecs_keyword syslog_kw[] = {
	{ "facility",
	  "name",
	  "Set syslog facility. Arg is one of the following: user, daemon, "
	  "auth, authpriv, mail, cron, local0 through local7 "
	  "(case-insensitive), or a facility number.",
	  grecs_type_string, GRECS_DFLT,
	  &log_facility, 0, cb_syslog_facility },
	{ "tag", "string", "Tag syslog messages with this string",
	  grecs_type_string, GRECS_DFLT,
	  &syslog_tag },
	{ NULL },
};

static int
cb_service(enum grecs_callback_command cmd, grecs_node_t *node,
	   void *varptr, void *cb_data)
{
	grecs_locus_t *locus = &node->locus;
	grecs_value_t *value = node->v.value;
	
	if (stv_assert_string_arg(locus, cmd, value))
		return 1;
	return service_add(value->v.string, locus);
}

static struct grecs_keyword tallymand_kw[] = {
	{ "listen", "socket", "Listen on this address",
	  grecs_type_string /* FIXME: consider grecs_type_sockaddr */,
	  GRECS_DFLT, &listen_address, },
	{ "pidfile", "file", "Store PID in that file",
	  grecs_type_string, GRECS_DFLT, &pidfile },
	{ "user", "name|+uid", "Run as this user",
	  grecs_type_string, GRECS_DFLT, &runas_user },
	{ "group", "name|+gid", "Run with this group privileges",
	  grecs_type_string, GRECS_DFLT, &runas_group },
	{ "syslog", NULL, "Configure syslog logging",
	  grecs_type_section, GRECS_DFLT, NULL, 0, NULL, NULL, syslog_kw },
	{ "service", "name", "Define service",
	  grecs_type_string, GRECS_DFLT, NULL, 0, cb_service },
	{ "instance-state-ttl", "seconds", "Sets instance state TTL",
	  grecs_type_uint, GRECS_DFLT, &ttl_instance_state },
	{ "hostproc-server", "host",
	  "Sets hostname or IP address of the hostproc agent",
	  grecs_type_string, GRECS_DFLT, &hostproc_server_addr },
	{ "snmp-client-config", "file",
	  "The filename of the SNMP client configuration file containing"
	  " access credentials for hostproc-server",
	  grecs_type_string, GRECS_DFLT, &snmp_client_config_file },
	{ NULL }
};

void
config_help(void)
{
	static char docstring[] =
		"Configuration file structure for stevedore.\n"
		"For more information, see stevedore(8).";
	grecs_print_docstring(docstring, 0, stdout);
	grecs_print_statement_array(tallymand_kw, 1, 0, stdout);
}

void
readconfig(char const *file)
{
	struct grecs_node *tree;

	grecs_log_to_stderr = 1;
	tree = grecs_parse(file);
	if (!tree)
		exit(1);
	if (grecs_tree_process(tree, tallymand_kw))
		exit(1);

	if (!syslog_tag)
		syslog_tag = progname;

	if (!listen_address)
		listen_address = DEFAULT_ADDRESS ":" DEFAULT_PORT;
}
